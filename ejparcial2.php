<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Matriz Cuadrada</title>
</head>

<body>
    <?php
    function generarMatriz($tamano)
    {
        $matriz = [];
        for ($i = 0; $i < $tamano; $i++) {
            for ($j = 0; $j < $tamano; $j++) {
                $matriz[$i][$j] = rand(0, 9);
            }
        }
        return $matriz;
    }

    function imprimirMatriz($matriz)
    {
        echo "<table border='1'>";
        foreach ($matriz as $fila) {
            echo "<tr>";
            foreach ($fila as $valor) {
                echo "<td>$valor</td>";
            }
            echo "</tr>";
        }
        echo "</table>";
    }

    function calcularSumaDiagonal($matriz)
    {
        $suma = 0;
        for ($i = 0; $i < count($matriz); $i++) {
            $suma += $matriz[$i][$i];
        }
        return $suma;
    }

    define("TAMANO_MATRIZ", 3);
    $sumaDiagonal = 0;

    do {
        $matriz = generarMatriz(TAMANO_MATRIZ);
        imprimirMatriz($matriz);
        $sumaDiagonal = calcularSumaDiagonal($matriz);
        echo "<p>Suma de la diagonal principal: $sumaDiagonal</p>";
    } while ($sumaDiagonal < 10 || $sumaDiagonal > 15);

    echo "<p>La suma de la diagonal está entre 10 y 15. Fin del script.</p>";
    ?>

</body>

</html>
